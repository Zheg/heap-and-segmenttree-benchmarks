﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using OfficeOpenXml;
using OfficeOpenXml.Drawing.Chart;
using static BenchmarkSegmentTree.Constants;

namespace PlotBuilderSegmentTree
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            var path = "Benchmark.xlsx";
            try
            {
                var file = File.Open(path, FileMode.Create);
                file.Close();
            }
            catch (IOException)
            {
                Console.WriteLine("Не удаётся получить доступ к файлу " + path +
                                  "\nВозможно он используется другой программой");
                return;
            }

            var package = new ExcelPackage();

            var pathToFilesNet = "D:\\RiderProjects\\Benchmark\\BenchmarkSegmentTree\\bin\\Release\\";
            var pathToFilesCore = "D:\\RiderProjects\\Benchmark\\BenchmarkSegmentTreeCore\\bin\\Release\\net472\\";

            var sheetNet = package.Workbook.Worksheets
                .Add(".Net");
            BuildPlot(sheetNet, pathToFilesNet + "BenchmarksSegmentTree.csv");

            var sheetCore = package.Workbook.Worksheets
                .Add("Core");
            BuildPlot(sheetCore, pathToFilesCore + "BenchmarksSegmentTree.csv");

            var sheetComparison = package.Workbook.Worksheets
                .Add("Comparison");
            PlatformComparison(sheetComparison, pathToFilesNet + "BenchmarksSegmentTree.csv",
                pathToFilesCore + "BenchmarksSegmentTree.csv");

            File.WriteAllBytes(path, package.GetAsByteArray());
        }

        public static double[,] AverageResults(string pathToData)
        {
            const int colCount = 5;
            var results = new double[Orders, colCount];

            using (var sr = new StreamReader(pathToData))
            {
                for (var i = 0; i < Orders; i++)
                for (var j = 0; j < colCount; j++)
                    results[i, j] = 0;

                for (var i = 0; i < Repeats; i++)
                for (var j = 0; j < Orders; j++)
                {
                    var line = sr.ReadLine();
                    var values = line.Split(';').Select(long.Parse).ToArray();
                    for (var k = 0; k < values.Length; k++)
                        results[j, k] += values[k];
                }

                for (var i = 0; i < Orders; i++)
                for (var j = 0; j < colCount; j++)
                {
                    results[i, j] = Math.Round(results[i, j] / Repeats, 2);
                }
            }

            return results;
        }
        public static void PlatformComparison(ExcelWorksheet sheet, string pathToDataNet, string pathToDataCore)
        {
            const int colCount = 5;
            var net = AverageResults(pathToDataNet);
            var core = AverageResults(pathToDataCore);

            sheet.Cells[1, 1].Value = "Размерность";
            sheet.Cells[1, 2].Value = "Построение";
            sheet.Cells[1, 3].Value = "Полная сумма";
            sheet.Cells[1, 4].Value = "Левая половина";
            sheet.Cells[1, 5].Value = "Центр";

            for (var i = 0; i < Orders; i++)
            for (var j = 0; j < colCount; j++)
            {
                sheet.Cells[i + 2, j + 1].Value = net[i, j];
                sheet.Cells[i + 2 + Orders, j + 1].Value = core[i, j];
            }

            var buildChart = sheet.Drawings.AddChart("build", eChartType.Line);
            buildChart.Title.Text = "Построение";
            buildChart.SetPosition(1, 0, 6, 0);
            buildChart.SetSize(600, 300);

            var fullChart = sheet.Drawings.AddChart("full", eChartType.Line);
            fullChart.Title.Text = "Полная сумма";
            fullChart.SetPosition(16, 0, 6, 0);
            fullChart.SetSize(600, 300);

            var halfChart = sheet.Drawings.AddChart("half", eChartType.Line);
            halfChart.Title.Text = "Левая половина";
            halfChart.SetPosition(31, 0, 6, 0);
            halfChart.SetSize(600, 300);

            var centerChart = sheet.Drawings.AddChart("center", eChartType.Line);
            centerChart.Title.Text = "Центр";
            centerChart.SetPosition(46, 0, 6, 0);
            centerChart.SetSize(600, 300);

            buildChart.YAxis.LogBase = 10;
            fullChart.YAxis.LogBase = 10;
            halfChart.YAxis.LogBase = 10;
            centerChart.YAxis.LogBase = 10;

            {
                var buildData = buildChart.Series.Add(
                    sheet.Cells[2, 2, Orders + 1, 2],
                    sheet.Cells[2, 1, Orders + 1, 1]);
                buildData.Header = ".Net";

                var fullData = fullChart.Series.Add(
                    sheet.Cells[2, 3, Orders + 1, 3],
                    sheet.Cells[2, 1, Orders + 1, 1]);
                fullData.Header = ".Net";

                var halfData = halfChart.Series.Add(
                    sheet.Cells[2, 4, Orders + 1, 4],
                    sheet.Cells[2, 1, Orders + 1, 1]);
                halfData.Header = ".Net";

                var centerData = centerChart.Series.Add(
                    sheet.Cells[2, 5, Orders + 1, 5],
                    sheet.Cells[2, 1, Orders + 1, 1]);
                centerData.Header = ".Net";
            }
            {
                var buildData = buildChart.Series.Add(
                    sheet.Cells[2 + Orders, 2, Orders + 1 + Orders, 2],
                    sheet.Cells[2 + Orders, 1, Orders + 1 + Orders, 1]);
                buildData.Header = "Core";

                var fullData = fullChart.Series.Add(
                    sheet.Cells[2 + Orders, 3, Orders + 1 + Orders, 3],
                    sheet.Cells[2 + Orders, 1, Orders + 1 + Orders, 1]);
                fullData.Header = "Core";

                var halfData = halfChart.Series.Add(
                    sheet.Cells[2 + Orders, 4, Orders + 1 + Orders, 4],
                    sheet.Cells[2 + Orders, 1, Orders + 1 + Orders, 1]);
                halfData.Header = "Core";

                var centerData = centerChart.Series.Add(
                    sheet.Cells[2 + Orders, 5, Orders + 1 + Orders, 5],
                    sheet.Cells[2 + Orders, 1, Orders + 1 + Orders, 1]);
                centerData.Header = "Core";
            }
        }

        public static void BuildPlot(ExcelWorksheet sheet, string pathToData)
        {
            var buildChart = sheet.Drawings.AddChart("build", eChartType.Line);
            buildChart.Title.Text = "Построение";
            buildChart.SetPosition(1, 0, 6, 0);
            buildChart.SetSize(600, 300);

            var fullChart = sheet.Drawings.AddChart("full", eChartType.Line);
            fullChart.Title.Text = "Полная сумма";
            fullChart.SetPosition(16, 0, 6, 0);
            fullChart.SetSize(600, 300);

            var halfChart = sheet.Drawings.AddChart("half", eChartType.Line);
            halfChart.Title.Text = "Левая половина";
            halfChart.SetPosition(31, 0, 6, 0);
            halfChart.SetSize(600, 300);

            var centerChart = sheet.Drawings.AddChart("center", eChartType.Line);
            centerChart.Title.Text = "Центр";
            centerChart.SetPosition(46, 0, 6, 0);
            centerChart.SetSize(600, 300);

            buildChart.YAxis.LogBase = 10;
            fullChart.YAxis.LogBase = 10;
            halfChart.YAxis.LogBase = 10;
            centerChart.YAxis.LogBase = 10;

            sheet.Cells[1, 1].Value = "Размерность";
            sheet.Cells[1, 2].Value = "Построение";
            sheet.Cells[1, 3].Value = "Полная сумма";
            sheet.Cells[1, 4].Value = "Левая половина";
            sheet.Cells[1, 5].Value = "Центр";

            using (var sr = new StreamReader(pathToData))
            {
                for (var i = 0; i < Repeats; i++)
                {
                    for (var j = 0; j < Orders; j++)
                    {
                        var line = sr.ReadLine();
                        var values = line.Split(';').Select(int.Parse).ToArray();
                        for (var k = 0; k < values.Length; k++)
                            sheet.Cells[i * Orders + j + 2, k + 1].Value = values[k];
                    }

                    var buildData = buildChart.Series.Add(
                        sheet.Cells[i * Orders + 2, 2, (i + 1) * Orders + 1, 2],
                        sheet.Cells[i * Orders + 2, 1, (i + 1) * Orders + 1, 1]);
                    buildData.Header = "Bench" + (i + 1);

                    var fullData = fullChart.Series.Add(
                        sheet.Cells[i * Orders + 2, 3, (i + 1) * Orders + 1, 3],
                        sheet.Cells[i * Orders + 2, 1, (i + 1) * Orders + 1, 1]);
                    fullData.Header = "Bench" + (i + 1);

                    var halfData = halfChart.Series.Add(
                        sheet.Cells[i * Orders + 2, 4, (i + 1) * Orders + 1, 4],
                        sheet.Cells[i * Orders + 2, 1, (i + 1) * Orders + 1, 1]);
                    halfData.Header = "Bench" + (i + 1);

                    var centerData = centerChart.Series.Add(
                        sheet.Cells[i * Orders + 2, 5, (i + 1) * Orders + 1, 5],
                        sheet.Cells[i * Orders + 2, 1, (i + 1) * Orders + 1, 1]);
                    centerData.Header = "Bench" + (i + 1);
                }
            }
        }
    }
}