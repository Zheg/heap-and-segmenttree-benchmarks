﻿using System;

namespace BenchmarkCore
{
    public static class ArrayGenerator
    {
        public static int[] Sorted(int size)
        {
            var array = new int[size];
            for (int i = 0; i < size; i++)
            {
                array[i] = i;
            }

            return array;
        }
        
        public static int[] Reversed(int size)
        {
            var array = new int[size];
            for (int i = 0; i < size; i++)
            {
                array[i] = size - i - 1;
            }

            return array;
        }
        
        public static int[] RandomBounded(int size)
        {
            var rnd = new Random();
            var array = new int[size];
            for (int i = 0; i < size; i++)
            {
                array[i] = rnd.Next(size);
            }

            return array;
        }
        
        public static int[] Random(int size)
        {
            var rnd = new Random();
            var array = new int[size];
            for (int i = 0; i < size; i++)
            {
                array[i] = rnd.Next();
            }

            return array;
        }
        
        public enum ArrayType
        {
            Sorted = 0,
            Reversed,
            RandomBounded,
            Random,
            Heap
        }

        public static int[] Generate(ArrayType type, int size)
        {
            int[] array;
            switch (type)
            {
                case ArrayType.Sorted:
                    array = ArrayGenerator.Sorted(size);
                    break;
                case ArrayType.Reversed:
                    array = ArrayGenerator.Reversed(size);
                    break;
                case ArrayType.RandomBounded:
                    array = ArrayGenerator.RandomBounded(size);
                    break;
                case ArrayType.Random:
                    array = ArrayGenerator.Random(size);
                    break;
                case ArrayType.Heap:
                    array = ArrayGenerator.Reversed(size);
                    var heap = new BinaryHeap();
                    heap.BuildHeap(array);
                    array = heap.GetArray();
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }

            return array;
        }
    }
}