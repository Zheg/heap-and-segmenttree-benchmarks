﻿using System.Collections.Generic;
using System.Linq;

namespace Benchmark
{
    public class BinaryHeap
    {
        private List<int> _list;

        public int HeapSize
        {
            get
            {
                return this._list.Count();
            }
        }
        
        public void Add(int value)
        {
            _list.Add(value);
            int i = HeapSize - 1;
            int parent = (i - 1) / 2;

            while (i > 0 && _list[parent] < _list[i])
            {
                int temp = _list[i];
                _list[i] = _list[parent];
                _list[parent] = temp;

                i = parent;
                parent = (i - 1) / 2;
            }
        }
        
        public void Heapify(int i)
        {
            int leftChild;
            int rightChild;
            int largestChild;

            for (; ; )
            {
                leftChild = 2 * i + 1;
                rightChild = 2 * i + 2;
                largestChild = i;

                if (leftChild < HeapSize && _list[leftChild] > _list[largestChild]) 
                {
                    largestChild = leftChild;
                }

                if (rightChild < HeapSize && _list[rightChild] > _list[largestChild])
                {
                    largestChild = rightChild;
                }

                if (largestChild == i) 
                {
                    break;
                }

                int temp = _list[i];
                _list[i] = _list[largestChild];
                _list[largestChild] = temp;
                i = largestChild;
            }
        }
        
        public void BuildHeap(int[] sourceArray)
        {
            _list = sourceArray.ToList();
            for (int i = HeapSize / 2; i >= 0; i--)
            {
                Heapify(i);
            }
        }
        
        public int GetMax()
        {
            int result = _list[0];
            _list[0] = _list[HeapSize - 1];
            _list.RemoveAt(HeapSize - 1);
            Heapify(0);
            return result;
        }
        
        public void HeapSort(int[] array)
        {
            BuildHeap(array);
            for (int i = array.Length - 1; i >= 0; i--)
            {
                array[i] = GetMax();
                Heapify(0);
            }
        }

        public int[] GetArray()
        {
            return _list.ToArray();
        }
    }
}