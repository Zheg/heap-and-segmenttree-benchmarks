﻿namespace Benchmark
{
    public static class Constants
    {
        public const int Orders = 7;
        public const int Repeats = 255;
        public const int ArrayTypeCount = 5;
    }
}