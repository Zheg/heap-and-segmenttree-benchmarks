﻿using System.Diagnostics;
using System.IO;
using System.Text;
using static Benchmark.Constants;

namespace Benchmark
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            {
                // Первый вызов происходит очень долго
                // Преднамернный вызов здесь, чтобы не отображалось на бенчмарках
                var heap = new BinaryHeap();
                heap.BuildHeap(new[] {1, 3});
                heap.Add(2);
                heap.GetMax();
            }
            for (var i = 0; i < ArrayTypeCount; i++)
            {
                var results = Bench((ArrayGenerator.ArrayType) i);
                WriteToFile("Benchmarks" + i + ".csv", results);
            }

            // Bench(ArrayGenerator.ArrayType.Sorted);
            // Bench(ArrayGenerator.ArrayType.Reversed);
            // Bench(ArrayGenerator.ArrayType.RandomBounded);
            // Bench(ArrayGenerator.ArrayType.Random);
            // Bench(ArrayGenerator.ArrayType.Heap);
        }

        private static long[,] Bench(ArrayGenerator.ArrayType type)
        {
            var results = new long[Repeats * Orders, 4];
            for (var i = 0; i < Repeats; i++)
            {
                var size = 10;
                for (var j = 0; j < Orders; j++, size *= 10)
                {
                    results[i * Orders + j, 0] = size;
                    var array = ArrayGenerator.Generate(type, size);

                    var heap = new BinaryHeap();

                    var stopwatch = new Stopwatch();
                    stopwatch.Start();
                    stopwatch.Stop();

                    // Построение
                    stopwatch.Restart();
                    heap.BuildHeap(array);
                    stopwatch.Stop();
                    results[i * Orders + j, 1] = stopwatch.ElapsedTicks;

                    // Добавление 
                    stopwatch.Restart();
                    heap.Add(size / 2);
                    stopwatch.Stop();
                    results[i * Orders + j, 2] = stopwatch.ElapsedTicks;

                    // Удаление
                    stopwatch.Restart();
                    heap.GetMax();
                    stopwatch.Stop();
                    results[i * Orders + j, 3] = stopwatch.ElapsedTicks;
                }
            }

            return results;
        }

        public static void WriteToFile(string path, long[,] data)
        {
            var lines = new string[data.GetLength(0)];
            for (var i = 0; i < data.GetLength(0); i++)
            {
                var line = "";
                for (var j = 0; j < data.GetLength(1); j++) line += data[i, j] + ";";

                line = line.Remove(line.Length - 1);
                lines[i] = line;
            }

            File.WriteAllLines(path, lines, Encoding.Default);
        }
    }
}