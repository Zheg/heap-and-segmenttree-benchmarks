﻿using System;
using System.IO;
using System.Linq;
using Benchmark;
using OfficeOpenXml;
using OfficeOpenXml.Drawing.Chart;
using static Benchmark.Constants;

namespace PlotBuilder
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            var path = "Benchmark.xlsx";
            try
            {
                var file = File.Open(path, FileMode.Create);
                file.Close();
            }
            catch (IOException)
            {
                Console.WriteLine("Не удаётся получить доступ к файлу " + path +
                                  "\nВозможно он используется другой программой");
                return;
            }


            var pathToFilesNet = "D:\\RiderProjects\\Benchmark\\Benchmark\\bin\\Release\\";
            var pathToFilesCore = "D:\\RiderProjects\\Benchmark\\BenchmarkCore\\bin\\Release\\net472\\";
            BuildAllPlots(pathToFilesNet, "BenchmarkNet.xlsx");
            BuildAllPlots(pathToFilesCore, "BenchmarkCore.xlsx");
            PlatformComparison(pathToFilesNet, pathToFilesCore, "PlatformComparison.xlsx");
        }

        public static void BuildPlatformComparisonPlot(ExcelWorksheet sheet, string pathToFileNet,
            string pathToFileCore)
        {
            sheet.Cells[1, 1].Value = "Размерность";
            sheet.Cells[1, 2].Value = "Построение";
            sheet.Cells[1, 3].Value = "Полная сумма";
            sheet.Cells[1, 4].Value = "Левая половина";
            sheet.Cells[1, 5].Value = "Центр";

            var net = AverageResults(pathToFileNet);
            var core = AverageResults(pathToFileCore);

            for (var i = 0; i < Orders; i++)
            for (var j = 0; j < net.GetLength(1); j++)
            {
                sheet.Cells[i + 2, j + 1].Value = net[i, j];
                sheet.Cells[i + 2 + Orders, j + 1].Value = core[i, j];
            }

            var buildChart = sheet.Drawings.AddChart("build", eChartType.Line);
            buildChart.Title.Text = "Построение";
            buildChart.SetPosition(1, 0, 6, 0);
            buildChart.SetSize(600, 300);

            var addChart = sheet.Drawings.AddChart("add", eChartType.Line);
            addChart.Title.Text = "Добавление";
            addChart.SetPosition(16, 0, 6, 0);
            addChart.SetSize(600, 300);

            var getChart = sheet.Drawings.AddChart("get", eChartType.Line);
            getChart.Title.Text = "Извлечение";
            getChart.SetPosition(31, 0, 6, 0);
            getChart.SetSize(600, 300);

            buildChart.YAxis.LogBase = 10;
            addChart.YAxis.LogBase = 10;
            getChart.YAxis.LogBase = 10;

            {
                var buildData = buildChart.Series.Add(
                    sheet.Cells[2, 2, Orders + 1, 2],
                    sheet.Cells[2, 1, Orders + 1, 1]);
                buildData.Header = ".Net";

                var addData = addChart.Series.Add(
                    sheet.Cells[2, 3, Orders + 1, 3],
                    sheet.Cells[2, 1, Orders + 1, 1]);
                addData.Header = ".Net";

                var getData = getChart.Series.Add(
                    sheet.Cells[2, 4, Orders + 1, 4],
                    sheet.Cells[2, 1, Orders + 1, 1]);
                getData.Header = ".Net";
            }
            {
                var buildData = buildChart.Series.Add(
                    sheet.Cells[2 + Orders, 2, Orders + 1 + Orders, 2],
                    sheet.Cells[2 + Orders, 1, Orders + 1 + Orders, 1]);
                buildData.Header = "Core";

                var addData = addChart.Series.Add(
                    sheet.Cells[2 + Orders, 3, Orders + 1 + Orders, 3],
                    sheet.Cells[2 + Orders, 1, Orders + 1 + Orders, 1]);
                addData.Header = "Core";

                var getData = getChart.Series.Add(
                    sheet.Cells[2 + Orders, 4, Orders + 1 + Orders, 4],
                    sheet.Cells[2 + Orders, 1, Orders + 1 + Orders, 1]);
                getData.Header = "Core";
            }
        }

        public static void PlatformComparison(string pathToFilesNet, string pathToFilesCore, string pathToSave)
        {
            var package = new ExcelPackage();

            var number = 0;
            var sheetSorted = package.Workbook.Worksheets
                .Add("Сортированный массив");
            var fileName = "Benchmarks" + number++ + ".csv";
            BuildPlatformComparisonPlot(sheetSorted, pathToFilesNet + fileName, pathToFilesCore + fileName);

            var sheetReversed = package.Workbook.Worksheets
                .Add("Сортированный в обратном порядке массив");
            fileName = "Benchmarks" + number++ + ".csv";
            BuildPlatformComparisonPlot(sheetReversed, pathToFilesNet + fileName, pathToFilesCore + fileName);

            var sheetRandomBounded = package.Workbook.Worksheets
                .Add("Случайный массив, значения ограничены размером");
            fileName = "Benchmarks" + number++ + ".csv";
            BuildPlatformComparisonPlot(sheetRandomBounded, pathToFilesNet + fileName, pathToFilesCore + fileName);

            var sheetRandom = package.Workbook.Worksheets
                .Add("Случайный массив");
            fileName = "Benchmarks" + number++ + ".csv";
            BuildPlatformComparisonPlot(sheetRandom, pathToFilesNet + fileName, pathToFilesCore + fileName);

            var sheetHeep = package.Workbook.Worksheets
                .Add("Массив в виде кучи");
            fileName = "Benchmarks" + number++ + ".csv";
            BuildPlatformComparisonPlot(sheetHeep, pathToFilesNet + fileName, pathToFilesCore + fileName);

            File.WriteAllBytes(pathToSave, package.GetAsByteArray());
        }

        public static double[,] AverageResults(string pathToData)
        {
            const int colCount = 4;
            var results = new double[Orders, colCount];

            using (var sr = new StreamReader(pathToData))
            {
                for (var i = 0; i < Orders; i++)
                for (var j = 0; j < colCount; j++)
                    results[i, j] = 0;

                for (var i = 0; i < Repeats; i++)
                for (var j = 0; j < Orders; j++)
                {
                    var line = sr.ReadLine();
                    var values = line.Split(';').Select(long.Parse).ToArray();
                    for (var k = 0; k < values.Length; k++)
                        results[j, k] += values[k];
                }

                for (var i = 0; i < Orders; i++)
                for (var j = 0; j < colCount; j++)
                {
                    results[i, j] = Math.Round(results[i, j] / Repeats, 2);
                }
            }

            return results;
        }

        public static void BuildAllPlots(string pathToFiles, string pathToSave)
        {
            var package = new ExcelPackage();

            var number = 0;
            var sheetSorted = package.Workbook.Worksheets
                .Add("Сортированный массив");
            BuildPlot(sheetSorted, pathToFiles + "Benchmarks" + number++ + ".csv");

            var sheetReversed = package.Workbook.Worksheets
                .Add("Сортированный в обратном порядке массив");
            BuildPlot(sheetReversed, pathToFiles + "Benchmarks" + number++ + ".csv");

            var sheetRandomBounded = package.Workbook.Worksheets
                .Add("Случайный массив, значения ограничены размером");
            BuildPlot(sheetRandomBounded, pathToFiles + "Benchmarks" + number++ + ".csv");

            var sheetRandom = package.Workbook.Worksheets
                .Add("Случайный массив");
            BuildPlot(sheetRandom, pathToFiles + "Benchmarks" + number++ + ".csv");

            var sheetHeep = package.Workbook.Worksheets
                .Add("Массив в виде кучи");
            BuildPlot(sheetHeep, pathToFiles + "Benchmarks" + number++ + ".csv");

            var sheetAll = package.Workbook.Worksheets
                .Add("Сравнение разных входных данных");
            BuildComparison(sheetAll, pathToFiles);

            File.WriteAllBytes(pathToSave, package.GetAsByteArray());
        }

        public static void BuildComparison(ExcelWorksheet sheet, string pathToData)
        {
            var buildChart = sheet.Drawings.AddChart("build", eChartType.Line);
            buildChart.Title.Text = "Построение";
            buildChart.SetPosition(1, 0, 6, 0);
            buildChart.SetSize(600, 300);

            var addChart = sheet.Drawings.AddChart("add", eChartType.Line);
            addChart.Title.Text = "Добавление";
            addChart.SetPosition(16, 0, 6, 0);
            addChart.SetSize(600, 300);

            var getChart = sheet.Drawings.AddChart("get", eChartType.Line);
            getChart.Title.Text = "Извлечение";
            getChart.SetPosition(31, 0, 6, 0);
            getChart.SetSize(600, 300);

            buildChart.YAxis.LogBase = 10;
            addChart.YAxis.LogBase = 10;
            getChart.YAxis.LogBase = 10;

            sheet.Cells[1, 1].Value = "Размерность";
            sheet.Cells[1, 2].Value = "Построение";
            sheet.Cells[1, 3].Value = "Добавление";
            sheet.Cells[1, 4].Value = "Извлечение";

            for (var number = 0; number < ArrayTypeCount; number++)
            {
                var results = AverageResults(pathToData + "Benchmarks" + number + ".csv");

                for (var j = 0; j < Orders; j++)
                for (var k = 0; k < results.GetLength(1); k++)
                    sheet.Cells[number * Orders + j + 2, k + 1].Value = results[j, k];

                var buildData = buildChart.Series.Add(
                    sheet.Cells[number * Orders + 2, 2, (number + 1) * Orders + 1, 2],
                    sheet.Cells[number * Orders + 2, 1, (number + 1) * Orders + 1, 1]);
                buildData.Header = ((ArrayGenerator.ArrayType) number).ToString();

                var addData = addChart.Series.Add(
                    sheet.Cells[number * Orders + 2, 3, (number + 1) * Orders + 1, 3],
                    sheet.Cells[number * Orders + 2, 1, (number + 1) * Orders + 1, 1]);
                addData.Header = ((ArrayGenerator.ArrayType) number).ToString();

                var getData = getChart.Series.Add(
                    sheet.Cells[number * Orders + 2, 4, (number + 1) * Orders + 1, 4],
                    sheet.Cells[number * Orders + 2, 1, (number + 1) * Orders + 1, 1]);
                getData.Header = ((ArrayGenerator.ArrayType) number).ToString();
            }
        }

        public static void BuildPlot(ExcelWorksheet sheet, string pathToData)
        {
            var buildChart = sheet.Drawings.AddChart("build", eChartType.Line);
            buildChart.Title.Text = "Построение";
            buildChart.SetPosition(1, 0, 6, 0);
            buildChart.SetSize(600, 300);

            var addChart = sheet.Drawings.AddChart("add", eChartType.Line);
            addChart.Title.Text = "Добавление";
            addChart.SetPosition(16, 0, 6, 0);
            addChart.SetSize(600, 300);

            var getChart = sheet.Drawings.AddChart("get", eChartType.Line);
            getChart.Title.Text = "Извлечение";
            getChart.SetPosition(31, 0, 6, 0);
            getChart.SetSize(600, 300);

            buildChart.YAxis.LogBase = 10;
            addChart.YAxis.LogBase = 10;
            getChart.YAxis.LogBase = 10;

            sheet.Cells[1, 1].Value = "Размерность";
            sheet.Cells[1, 2].Value = "Построение";
            sheet.Cells[1, 3].Value = "Добавление";
            sheet.Cells[1, 4].Value = "Извлечение";

            using (var sr = new StreamReader(pathToData))
            {
                for (var i = 0; i < Repeats; i++)
                {
                    for (var j = 0; j < Orders; j++)
                    {
                        var line = sr.ReadLine();
                        var values = line.Split(';').Select(int.Parse).ToArray();
                        for (var k = 0; k < values.Length; k++)
                            sheet.Cells[i * Orders + j + 2, k + 1].Value = values[k];
                    }

                    var buildData = buildChart.Series.Add(
                        sheet.Cells[i * Orders + 2, 2, (i + 1) * Orders + 1, 2],
                        sheet.Cells[i * Orders + 2, 1, (i + 1) * Orders + 1, 1]);
                    buildData.Header = "Bench" + (i + 1);

                    var addData = addChart.Series.Add(
                        sheet.Cells[i * Orders + 2, 3, (i + 1) * Orders + 1, 3],
                        sheet.Cells[i * Orders + 2, 1, (i + 1) * Orders + 1, 1]);
                    addData.Header = "Bench" + (i + 1);

                    var getData = getChart.Series.Add(
                        sheet.Cells[i * Orders + 2, 4, (i + 1) * Orders + 1, 4],
                        sheet.Cells[i * Orders + 2, 1, (i + 1) * Orders + 1, 1]);
                    getData.Header = "Bench" + (i + 1);
                }
            }
        }
    }
}