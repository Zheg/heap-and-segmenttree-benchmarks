﻿namespace BenchmarkSegmentTreeCore
{
    public class SegmentTree
    {
        private int[] _tree;
        private int _shift;
        
        public void Build(int[] array)
        {
            _tree = new int[((array.Length + 1) / 2) * 4];
            _shift = _tree.Length / 2;
            for (int i = 0; i < array.Length; i++)
            {
                _tree[i + _shift] = array[i];
            }

            if (array.Length == 0)
            {
                _tree[_tree.Length - 1] = 0;
            }
            
            for (int i = _shift - 1; i >= 1; --i)
            {
                _tree[i] = _tree[i * 2] + _tree[i * 2 + 1];
            }
        }

        public int GetSum(int l, int r)
        {
            l += _shift;
            r += _shift;
            int sum = 0;
            while (l <= r)
            {
                if (l % 2 != 0) sum += _tree[l];
                if (r % 2 == 0) sum += _tree[r];
                l = (l + 1) / 2;
                r = (r - 1) / 2;
            }

            return sum;
        }
    }
}