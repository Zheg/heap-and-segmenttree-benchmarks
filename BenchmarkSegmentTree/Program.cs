﻿using System;
using System.Diagnostics;
using System.IO;
using static BenchmarkSegmentTree.Constants;

namespace BenchmarkSegmentTree
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            {
                // Первый вызов происходит очень долго
                // Преднамернный вызов здесь, чтобы не отображалось на бенчмарках
                var segmentTree = new SegmentTree();
                segmentTree.Build(new[] {1, 2});
                segmentTree.GetSum(0, 1);
            }
            var results = Bench();
            WriteToFile("BenchmarksSegmentTree.csv", results);
        }

        private static long[,] Bench()
        {
            var results = new long[Repeats * Orders, 5];
            for (var i = 0; i < Repeats; i++)
            {
                var size = 10;
                for (var j = 0; j < Orders; j++, size *= 10)
                {
                    results[i * Orders + j, 0] = size;
                    var array = RandomBounded(size);

                    var segmentTree = new SegmentTree();

                    var stopwatch = new Stopwatch();
                    stopwatch.Start();
                    stopwatch.Stop();

                    // Построение
                    stopwatch.Restart();
                    segmentTree.Build(array);
                    stopwatch.Stop();
                    results[i * Orders + j, 1] = stopwatch.ElapsedTicks;

                    // Сумма всего массива 
                    stopwatch.Restart();
                    segmentTree.GetSum(0, array.Length - 1);
                    stopwatch.Stop();
                    results[i * Orders + j, 2] = stopwatch.ElapsedTicks;

                    // Сумма левой половины
                    stopwatch.Restart();
                    segmentTree.GetSum(0, array.Length / 2);
                    stopwatch.Stop();
                    results[i * Orders + j, 3] = stopwatch.ElapsedTicks;

                    // Сумма центра
                    stopwatch.Restart();
                    segmentTree.GetSum(array.Length / 4, array.Length / 4 * 3);
                    stopwatch.Stop();
                    results[i * Orders + j, 4] = stopwatch.ElapsedTicks;
                }
            }

            return results;
        }

        public static int[] RandomBounded(int size)
        {
            var rnd = new Random();
            var array = new int[size];
            for (var i = 0; i < size; i++) array[i] = rnd.Next(10000);

            return array;
        }

        public static void WriteToFile(string path, long[,] data)
        {
            var lines = new string[data.GetLength(0)];
            for (var i = 0; i < data.GetLength(0); i++)
            {
                var line = "";
                for (var j = 0; j < data.GetLength(1); j++) line += data[i, j] + ";";

                line = line.Remove(line.Length - 1);
                lines[i] = line;
            }

            File.WriteAllLines(path, lines);
        }
    }
}